from django.urls import path
from .views import api_show_shoes, api_list_shoes

urlpatterns = [
    path("shoes/<int:id>/", api_show_shoes, name="api_show_shoes"),
    path("shoes/", api_list_shoes, name="api_list_shoes"),
]
