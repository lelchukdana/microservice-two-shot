from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_URL",
        "bin",
        "id",
    ]

    encoders = {"bin": BinVOEncoder()}


# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin_id = content['bin']
            bin = BinVO.objects.get(id=bin_id)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Incorrect Bin ID'},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                {"shoes": shoe},
                encoder=ShoeEncoder,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "The shoe ID does not exist"}, status=400
            )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                {"message": "Shoe is deleted"}, status=200
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"}, status=400
            )







    #         count, _ = Shoe.objects.filter(id=id).delete()
    #         return JsonResponse({"The shoe is deleted": count > 0})
    #     except Shoe.DoesNotExist:
    #         return JsonResponse({"message":"The shoe does not exist"})
    # else:  # if request is a PUT method
    #     content = json.loads(request.body)
    #     Shoe.objects.filter(id=id).update(**content)
    #     shoe = Shoe.objects.get(id=id)
    #     return JsonResponse(
    #         shoe,
    #         encoder=ShoeEncoder,
    #         safe=False
    #     )
