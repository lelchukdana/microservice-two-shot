import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [modelName, setModelName] = useState("");
    const [color, setColor] = useState("");
    const [pictureURL, setPictureURL] = useState("");
    const [bin, setBin] = useState("");


const fetchData = async () => {
    const url = `http://localhost:8100/api/shoes/`;
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json();
      setBins(data.bin)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.manufacturer = manufacturer;
    data.model_name = modelName;
    data.color = color;
    data.picture_URL = pictureURL;
    data.bin = bin;

    const url = `http://localhost:8080/api/bins/${bin}/shoes/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        const newShoe = await response.json()
        console.log(newShoe)

        setBins([]);
        setManufacturer("");
        setModelName("");
        setColor("");
        setPictureURL("");
        setBin("");
        }
    }

  const handleManufacturerChange = (event) => {
    const value = event.target.value
    setManufacturer(value);
  }

  const handleModelNameChange = (event) => {
    const value = event.target.value
    setModelName(value);
  }

  const handleColorChange = (event) => {
    const value = event.target.value
    setColor(value);
  }

  const handlePictureURLChange = (event) => {
    const value = event.target.value
    setPictureURL(value)
  }

  const handleBinChange = (event) => {
    const value = event.target.value
    setBin(value)
  }


    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-bin-form">
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelNameChange} value={modelName} placeholder="Model name" required name="model name" type="text" id="model-name" className="form-control" />
              <label htmlFor="model-name">Model name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureURLChange} value={pictureURL} placeholder="Picture" required name="picture" type="text" id="picture" className="form-control" />
              <label htmlFor="picture">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} value={bin} name="bin" className="form-select" required id="bin">
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ShoeForm;
