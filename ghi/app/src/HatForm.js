import React, { useEffect, useState } from 'react'

function HatForm() {
    const [location, setLocation] = useState([])
    const [fabric, setFabric] = useState('')
    const [style, setStyle] = useState('')
    const [color, setColor] = useState('')
    const [picture, setPicture] = useState('')
    const [selectedLocation, setSelectedLocation] = useState('')



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setLocation(data.locations)
        }
    }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }

    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setSelectedLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.fabric = fabric
        data.style_name = style
        data.color = color
        data.picture_url = picture
        data.location = selectedLocation

        const url = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newHat = await response.json()
            console.log(newHat)

            setFabric('')
            setStyle('')
            setColor('')
            setPicture('')
            setSelectedLocation('')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create New Hat</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} value={style} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} value={picture} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={selectedLocation} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {location.map(location => {
                                    return (
                                        <option key={location.id} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm
