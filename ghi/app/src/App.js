import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatForm from './HatForm'
import HatList from './HatList';
import Nav from './Nav';
import ShoeList from './ShoesList'
import ShoeForm from './ShoeForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="/" element={<MainPage />}></Route>
          <Route path ="shoes">
            <Route path="" element={<ShoeList />}></Route>
            <Route path="newshoe" element={<ShoeForm />}></Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
