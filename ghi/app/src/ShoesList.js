import React, { useEffect, useState } from 'react';

function ShoesList() {
    const[shoes, setShoes] = useState([])

    const getShoeData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    };

    const deleteShoes = async(id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {
                method: 'DELETE'
            })
            if (response.ok) {
                setShoes(shoes.filter((shoe) => shoe.id !== id))
            }
        } catch (error) {
            console.error('Oh no! The shoe could not be deleted :(', error);
        }

        }


    useEffect(()=>{
        getShoeData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Bin</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Delete Shoe</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoe) => {
                    return (
                        <tr key={shoe.id}>
                            <td> {shoe.manufacturer}</td>
                            <td>{shoe.modelName}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.pictureURL}</td>
                            <td>{shoe.delete}</td>
                            <tbutton onClick={() => deleteShoes(shoe.id)} className="btn btn-danger">Delete!</tbutton>
                        </tr>
                     );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;
