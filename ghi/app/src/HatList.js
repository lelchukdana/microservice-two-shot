import { useEffect, useState } from 'react'


function HatList() {
    const [hats, setHats] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')

        if (response.ok) {
            const data = await response.json()
            setHats(data.hats)
        }
    }

    const deleteHat = async (id) => {
        const response = await fetch(`http://localhost:8090/api/hats/${id}`, {
            method: 'DELETE',

        })

        if (response.ok) {
            setHats(hats.filter((hat) => hat.id !== id))
        }

    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>
                                <img src={hat.picture_url} alt={hat.picture_url} style={{ maxWidth: '100px', height: 'auto' }} />
                            </td>
                            <td>{hat.location}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteHat(hat.id)} />
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>

    )
}

export default HatList
